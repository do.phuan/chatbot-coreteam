# Graduation Thesis: Chatbot supporting Vietnamese

This project is the graduation thesis of our group. It aims to re-config Dialogflow to handle and extract Vietnamese queries for further processes. This bot could be deployed on various well-known messenger platforms such as Facebook Messenger, Slack, and Skype.

Main application will be deployed on Heroku Server and connected to web services through appropriate ports supported by Google cloud platform. 

## Rrerequisite

- Input file must be encoded with **utf-8*** and **.csv** format **_(see Update)_**
- Each line must follow the rules: 
    **entity_name, 1st word; 2nd word; n-th word** 
- Python3 must be installed
- Spintax must be installed
- Create Google Cloud APIs
- Create Heroku Account. 

## Setup

- Install Homebrew

```shell
$ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

- Install Python3

```shell
$ brew install python3
$ brew postinstall python3
```

- Install spintax
```shell
$ pip install spintax
```

## Update:
### 1st UPDATE: 
Training text file should be created on Linux and the code must be run on this OS as well. Running on Windows may cause some problems related to file encoding and eventually cannot import to dialogflow. 

### 2nd UPDATE:
We are now be able to generate each of training path by passing 3 options and its params before running generating script. There are 3 option flags

`-e` - create entity list from file. 

`-i` - create intent list

`-s` - create sample sentence to import to bot. 

**Notes:** Default directories of training files are **not available** from this update version. 

To run this script, please follow the syntax below: 
`./src/OneRun.sh -e_path-to-csv-file-of-entities_ -i_path-to-intent-file_ -s_path-to-sentence-file_`

Example 
`./src/OneRun.sh -e"../input/entity-list.csv" -s"sentence.csv"`

### 3rd UPDATE:
I just added the code to download all link and its title from HCMUS website. The links and the titles downloaded are stored in the directory: 
    `../site/all_link.csv`

To run this script, just type: `./downloadSite.sh`

## Directory structure
- **src:** the place that main code files are saved in.
- **input:** default input csv files. 
- **output** contains output files (include zip file and the directory that it is made from).
- **site** save all links and titles extracted from HCMUS portal.

## Features

This project supports the following functions:
- Crawl data from HCMUS students' portal for further processing
- Crawl data from TIKI.VN for online shopping chatbots
- Extract Vietnamese query by using pre-config dataset

## Authors

- Do Phu AN
- Hoang Minh QUAN
- Ho Thao HIEN

## License

[![License](https://img.shields.io/:license-gnu-blue.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)