USE STUDY_MANAGEMENT;

CREATE TABLE INTENT_RESPONSE (
    ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    INTENTTYPE INT,
    INTENT NVARCHAR(255), 
    RESPONSE NVARCHAR(255)
);

CREATE TABLE NGANH (
    ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    TENNGANH NVARCHAR(255)
);
CREATE TABLE CHUYENNGANH(
    ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    NGANH INT, 
    TENCHUYENNGANH NVARCHAR(255)
);

CREATE TABLE MONHOC (
    ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    TENMONHOC NVARCHAR(255), 
    GIAOVIEN INT
);

CREATE TABLE GIAOVIEN (
    ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    TENGIAOVIEN NVARCHAR(255)
);
CREATE TABLE NOIDUNGMONHOC (
    ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    MONHOC INT, 
    NOIDUNGMONHOC NVARCHAR(255)
);
CREATE TABLE TAILIEUMONHOC (
    ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    MONHOC INT,
    TENTAILIEU NVARCHAR(255),
    TAILIEU NVARCHAR(255)
);
select * from NOIDUNGCACBUOIHOC
CREATE TABLE NOIDUNGCACBUOIHOC (
    ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    MONHOC INT, 
    TUAN INT, 
    THOIGIAN NVARCHAR(255), 
    NOIDUNGBUOIHOC NVARCHAR(255)
);
CREATE TABLE BAITAP (
    ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    MONHOC INT, 
    NOIDUNGBAITAP NVARCHAR(255)
);
CREATE TABLE DIEMSO (
    ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    SINHVIEN INT, 
    MONHOC INT, 
    DIEM FLOAT
);
CREATE TABLE SINHVIEN (
    MSSV NVARCHAR(255) PRIMARY KEY, 
    TENSINHVIEN NVARCHAR(255),
    FACEBOOKID NVARCHAR(255)
);

CREATE TABLE CACHDANHGIA(
    ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    MONHOC INT,
    CACHDANHGIA NVARCHAR(255)
);
CREATE TABLE MONHOCTHAYTHE (
    MONHOC INT, 
    MONHOCTHAYTHE INT,
    PRIMARY KEY (MONHOC,MONHOCTHAYTHE)
);
CREATE TABLE MONHOCTRUOC (
    MONHOC INT, 
    MONHOCTRUOC INT,
    PRIMARY KEY (MONHOC,MONHOCTRUOC)
);
CREATE TABLE SINHVIEN_MONHOC (
    ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    SINHVIEN INT, 
    MONHOC INT
);

CREATE TABLE TAILIEU_MONHOC (
    ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    MONHOC INT, 
    TAILIEU INT
);

CREATE TABLE CHUYENNGANH_MONHOC (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	CHUYENNGANH INT,
    MONHOC INT, 
	HINHTHUCMONHOC INT # 1. chuyên ngành, 2.tự chọn, null.môn cơ sở
);


INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(1, 1, 'thong_tin_khac', null);
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(2, 2, 'noi_dung_mon_hoc', 'select ndmh.noidungmonhoc from noidungmonhoc as ndmh,monhoc as mh where ndmh.monhoc = mh.id and mh.tenmonhoc like ten_mon_hoc');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(3, 2, 'tai_lieu_mon_hoc', 'select tlmh.tentailieu,tlmh.tailieu from tailieumonhoc as tlmh, monhoc as mh where tlmh.monhoc = mh.id and mh.tenmonhoc like ten_mon_hoc');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(4, 2, 'mon_hoc_truoc', 'select mh.tenmonhoc from monhoc as mh where mh.id in (select distinct mht.monhoctruoc from monhoc as mh1, monhoctruoc as mht where mh1.id = mht.monhoc and mh1.tenmonhoc like ten_mon_hoc)');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(5, 2, 'mon_hoc_thay_the', 'select mh.tenmonhoc from monhoc as mh where mh.id in (select distinct mht.monhocthaythe from monhoc as mh1, monhocthaythe as mht where mh1.id = mht.monhoc and mh1.tenmonhoc like ten_mon_hoc)');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(6, 2, 'so_luong_mon_hoc_chuyen_nganh', 'select mh.tenmonhoc from monhoc as mh, chuyennganh as cn, chuyennganh_monhoc as cnmh where cnmh.chuyennganh = cn.id and cnmh.monhoc = mh.id and cn.tenchuyennganh like ten_chuyen_nganh and cnmh.hinhthucmonhoc = hinh_thuc_mon_hoc');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(7, 2, 'so_luong_chuyen_nganh', 'select * from nganh as n,chuyennganh as cn where n.id = cn.nganh and cn.tenchuyennganh like ten_chuyen_nganh');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(8, 2, 'noi_dung_buoi_hoc_tiep_theo', 'select mh.tenmonhoc, ndcbh.thoigian, ndcbh.noidungbuoihoc from noidungcacbuoihoc as ndcbh, monhoc as mh where ndcbh.monhoc = mh.id and ndcbh.thoigian > DATE(current_timestamp)');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(9, 2, 'diem_so', 'select mh.tenmonhoc, ds.diem from diemso as ds, monhoc as mh where ds.monhoc = mh.id and ds.sinhvien like ma_so_sinh_vien and mh.tenmonhoc like ten_mon_hoc');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(10, 2, 'cach_danh_gia', 'select cdg.cachdanhgia from cachdanhgia as cdg, monhoc as mh where cdg.monhoc = mh.id and mh.tenmonhoc like ten_mon_hoc');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(12, 2, 'so_luong_bai_tap', 'select bt.noidungbaitap from baitap as bt, monhoc as mh where bt.monhoc = mh.id and mh.tenmonhoc like ten_mon_hoc');

INSERT INTO NGANH(ID, TENNGANH) VALUES (1, N'Công nghệ thông tin');
INSERT INTO NGANH(ID, TENNGANH) VALUES (2, N'Toán');
INSERT INTO NGANH(ID, TENNGANH) VALUES (3, N'Vật lí');
INSERT INTO NGANH(ID, TENNGANH) VALUES (4, N'Hóa');
INSERT INTO NGANH(ID, TENNGANH) VALUES (5, N'Sinh');
INSERT INTO NGANH(ID, TENNGANH) VALUES (6, N'Địa chất');
INSERT INTO NGANH(ID, TENNGANH) VALUES (7, N'Công nghệ sinh học');
INSERT INTO NGANH(ID, TENNGANH) VALUES (8, N'Khoa học vật liệu');
INSERT INTO NGANH(ID, TENNGANH) VALUES (9, N'Điện tử viễn thông');
INSERT INTO NGANH(ID, TENNGANH) VALUES (10, N'Công nghệ môi trường');

INSERT INTO CHUYENNGANH(ID, NGANH, TENCHUYENNGANH) VALUES (1, 1, N'Công nghệ phần mềm');
INSERT INTO CHUYENNGANH(ID, NGANH, TENCHUYENNGANH) VALUES (2, 1, N'Hệ thống thông tin');
INSERT INTO CHUYENNGANH(ID, NGANH, TENCHUYENNGANH) VALUES (3, 1, N'Thị giác máy tính và robot');
INSERT INTO CHUYENNGANH(ID, NGANH, TENCHUYENNGANH) VALUES (4, 1, N'Mạng máy tính');
INSERT INTO CHUYENNGANH(ID, NGANH, TENCHUYENNGANH) VALUES (5, 1, N'Khoa học máy tính');
INSERT INTO CHUYENNGANH(ID, NGANH, TENCHUYENNGANH) VALUES (6, 1, N'Công nghệ tri thức');

INSERT INTO MONHOC(ID, TENMONHOC, GIAOVIEN) VALUES (1, 'Cơ sở dữ liệu', 2);
INSERT INTO MONHOC(ID, TENMONHOC, GIAOVIEN) VALUES (2, 'Khai thác dữ liệu', 2);
INSERT INTO MONHOC(ID, TENMONHOC, GIAOVIEN) VALUES (3, 'Điện tử căn bản', 1);
INSERT INTO MONHOC(ID, TENMONHOC, GIAOVIEN) VALUES (4, 'Nhập môn lập trình', 4);
INSERT INTO MONHOC(ID, TENMONHOC, GIAOVIEN) VALUES (5, 'An ninh mạng', 5);
INSERT INTO MONHOC(ID, TENMONHOC, GIAOVIEN) VALUES (6, 'Kĩ thuật lập trình', 3);
INSERT INTO MONHOC(ID, TENMONHOC, GIAOVIEN) VALUES (7, 'Hệ điều hành', 5);
INSERT INTO MONHOC(ID, TENMONHOC, GIAOVIEN) VALUES (8, 'Mạng máy tính', 5);
INSERT INTO MONHOC(ID, TENMONHOC, GIAOVIEN) VALUES (9, 'Cấu trúc dữ liệu và giải thuật', 1);

INSERT INTO GIAOVIEN(ID, TENGIAOVIEN) VALUES (1, N'Phạm Nguyễn Cương');
INSERT INTO GIAOVIEN(ID, TENGIAOVIEN) VALUES (2, N'Nguyễn Tấn Trần Minh Thư');
INSERT INTO GIAOVIEN(ID, TENGIAOVIEN) VALUES (3, N'Nguyễn Trần Minh Thư');
INSERT INTO GIAOVIEN(ID, TENGIAOVIEN) VALUES (4, N'Trần Minh Triểt');
INSERT INTO GIAOVIEN(ID, TENGIAOVIEN) VALUES (5, N'Bùi Thị Danh');

INSERT INTO NOIDUNGMONHOC(ID, MONHOC, NOIDUNGMONHOC) VALUES (1, 1, N'Nội dung môn học cơ sở dữ liệu');
INSERT INTO NOIDUNGMONHOC(ID, MONHOC, NOIDUNGMONHOC) VALUES (2, 2, N'Nội dung môn học khai thác dữ liệu');
INSERT INTO NOIDUNGMONHOC(ID, MONHOC, NOIDUNGMONHOC) VALUES (3, 3, N'Nội dung môn học điện tử căn bản');
INSERT INTO NOIDUNGMONHOC(ID, MONHOC, NOIDUNGMONHOC) VALUES (4, 4, N'Nội dung môn học nhập môn lập trình');
INSERT INTO NOIDUNGMONHOC(ID, MONHOC, NOIDUNGMONHOC) VALUES (5, 5, N'Nội dung môn học an ninh mạng');
INSERT INTO NOIDUNGMONHOC(ID, MONHOC, NOIDUNGMONHOC) VALUES (6, 6, N'Nội dung môn học kĩ thuật lập trình');
INSERT INTO NOIDUNGMONHOC(ID, MONHOC, NOIDUNGMONHOC) VALUES (7, 7, N'Nội dung môn học hệ điều hành');
INSERT INTO NOIDUNGMONHOC(ID, MONHOC, NOIDUNGMONHOC) VALUES (8, 8, N'Nội dung môn học mạng máy tính');
INSERT INTO NOIDUNGMONHOC(ID, MONHOC, NOIDUNGMONHOC) VALUES (9, 9, N'Nội dung môn học cấu trúc dữ liêu và giải thuật');

INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (1, 1, N'Tài liệu môn học môn Cơ sở dữ liệu', N'Tài liệu 1');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (2, 1, N'Tài liệu môn học môn Cơ sở dữ liệu', N'Tài liệu 2');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (3, 2, N'Tài liệu môn học môn Khai thác dữ liệu', N'Tài liệu 3');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (4, 2, N'Tài liệu môn học môn Khai thác dữ liệu', N'Tài liệu 4');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (5, 3, N'Tài liệu môn học môn Điện tử căn bản', N'Tài liệu 5');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (6, 3, N'Tài liệu môn học môn Điện tử căn bản', N'Tài liệu 6');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (7, 4, N'Tài liệu môn học môn Nhập môn lập trình', N'Tài liệu 7');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (8, 4, N'Tài liệu môn học môn Nhập môn lập trình', N'Tài liệu 8');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (9, 5, N'Tài liệu môn học môn An ninh mạng', N'Tài liệu 9');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (10, 5, N'Tài liệu môn học môn An ninh mạng', N'Tài liệu 10');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (11, 6, N'Tài liệu môn học môn Kĩ thuật lập trình', N'Tài liệu 11');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (12, 6, N'Tài liệu môn học môn Kĩ thuật lập trình', N'Tài liệu 12');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (13, 7, N'Tài liệu môn học môn Hệ điều hành', N'Tài liệu 13');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (14, 7, N'Tài liệu môn học môn Hệ điều hành', N'Tài liệu 14');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (15, 8, N'Tài liệu môn học môn Mạng máy tính', N'Tài liệu 15');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (16, 8, N'Tài liệu môn học môn Mạng máy tính', N'Tài liệu 16');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (17, 9, N'Tài liệu môn học môn Cấu trúc dữ liệu và giải thuật', N'Tài liệu 17');
INSERT INTO TAILIEUMONHOC(ID, MONHOC, TENTAILIEU, TAILIEU) VALUES (18, 9, N'Tài liệu môn học môn Cấu trúc dữ liệu và giải thuật', N'Tài liệu 18');

INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (1, 1, 1, '2018-04-14 08:00:00', N'Nội dung buổi học thứ 1 - môn 1');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (2, 1, 2, '2018-04-21 08:00:00', N'Nội dung buổi học thứ 2 - môn 1');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (3, 1, 3, '2018-04-28 08:00:00', N'Nội dung buổi học thứ 3 - môn 1');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (4, 2, 1, '2018-04-14 14:00:00', N'Nội dung buổi học thứ 1 - môn 2');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (5, 2, 2, '2018-04-21 14:00:00', N'Nội dung buổi học thứ 2 - môn 2');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (6, 2, 3, '2018-04-28 14:00:00', N'Nội dung buổi học thứ 3 - môn 2');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (7, 3, 1, '2018-04-14 08:00:00', N'Nội dung buổi học thứ 1 - môn 3');
INSERT INTO (ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (8, 3, 2, '2018-04-21 08:00:00', N'Nội dung buổi học thứ 2 - môn 3');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (9, 3, 3, '2018-04-28 08:00:00', N'Nội dung buổi học thứ 3 - môn 3');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (10, 4, 1, '2018-04-14 14:00:00', N'Nội dung buổi học thứ 1 - môn 4');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (11, 4, 2, '2018-04-21 14:00:00', N'Nội dung buổi học thứ 2 - môn 4');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (12, 4, 3, '2018-04-28 14:00:00', N'Nội dung buổi học thứ 3 - môn 4');
INSERT INTO NOIDUNGCANOIDUNGCACBUOIHOCCBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (13, 5, 1, '2018-04-14 15:00:00', N'Nội dung buổi học thứ 1 - môn 5');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (14, 5, 2, '2018-04-21 15:00:00', N'Nội dung buổi học thứ 2 - môn 5');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (15, 5, 3, '2018-04-28 15:00:00', N'Nội dung buổi học thứ 3 - môn 5');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (16, 6, 1, '2018-04-14 09:00:00', N'Nội dung buổi học thứ 1 - môn 6');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (17, 6, 2, '2018-04-21 09:00:00', N'Nội dung buổi học thứ 2 - môn 6');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (18, 6, 3, '2018-04-28 09:00:00', N'Nội dung buổi học thứ 3 - môn 6');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (19, 7, 1, '2018-04-14 08:00:00', N'Nội dung buổi học thứ 1 - môn 7');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (20, 7, 2, '2018-04-21 08:00:00', N'Nội dung buổi học thứ 2 - môn 7');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (21, 7, 3, '2018-04-28 08:00:00', N'Nội dung buổi học thứ 3 - môn 7');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (22, 8, 1, '2018-04-14 09:00:00', N'Nội dung buổi học thứ 1 - môn 8');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (23, 8, 2, '2018-04-21 09:00:00', N'Nội dung buổi học thứ 2 - môn 8');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (24, 8, 3, '2018-04-28 09:00:00', N'Nội dung buổi học thứ 3 - môn 8');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (25, 9, 1, '2018-04-14 08:00:00', N'Nội dung buổi học thứ 1 - môn 9');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (26, 9, 2, '2018-04-21 08:00:00', N'Nội dung buổi học thứ 2 - môn 9');
INSERT INTO NOIDUNGCACBUOIHOC(ID, MONHOC, TUAN, THOIGIAN, NOIDUNGBUOIHOC) VALUES (27, 9, 3, '2018-04-28 08:00:00', N'Nội dung buổi học thứ 3 - môn 9');

INSERT INTO BAITAP(ID, MONHOC, NOIDUNGBAITAP) VALUES (1, 1, 'Bài tập môn cơ sở dữ liệu');
INSERT INTO BAITAP(ID, MONHOC, NOIDUNGBAITAP) VALUES (2, 2, 'Bài tập môn khai thác dữ liệu');
INSERT INTO BAITAP(ID, MONHOC, NOIDUNGBAITAP) VALUES (3, 3, 'Bài tập môn điện tử căn bản');
INSERT INTO BAITAP(ID, MONHOC, NOIDUNGBAITAP) VALUES (4, 4, 'Bài tập môn nhập môn lập trình');
INSERT INTO BAITAP(ID, MONHOC, NOIDUNGBAITAP) VALUES (5, 5, 'Bài tập môn an ninh mạng');
INSERT INTO BAITAP(ID, MONHOC, NOIDUNGBAITAP) VALUES (6, 6, 'Bài tập môn kĩ thuật lập trình');
INSERT INTO BAITAP(ID, MONHOC, NOIDUNGBAITAP) VALUES (7, 7, 'Bài tập môn hệ điều hành');
INSERT INTO BAITAP(ID, MONHOC, NOIDUNGBAITAP) VALUES (8, 8, 'Bài tập môn mạng máy tính');
INSERT INTO BAITAP(ID, MONHOC, NOIDUNGBAITAP) VALUES (9, 9, 'Bài tập môn cấu trúc dữ liêu và giải thuật');
INSERT INTO BAITAP(ID, MONHOC, NOIDUNGBAITAP) VALUES (10, 1, 'Bài tập môn cơ sở dữ liệu 2');

INSERT INTO DIEMSO(ID, SINHVIEN, MONHOC, DIEM) VALUES (1, '1412169', 1, 9 );
INSERT INTO DIEMSO(ID, SINHVIEN, MONHOC, DIEM) VALUES (2, '1412169', 2, 8 );
INSERT INTO DIEMSO(ID, SINHVIEN, MONHOC, DIEM) VALUES (3, '1412169', 3, 6.5 );
INSERT INTO DIEMSO(ID, SINHVIEN, MONHOC, DIEM) VALUES (4, '1412169', 4, 9.5 );
INSERT INTO DIEMSO(ID, SINHVIEN, MONHOC, DIEM) VALUES (5, '1412169', 5, 7.5 );
INSERT INTO DIEMSO(ID, SINHVIEN, MONHOC, DIEM) VALUES (6, '1412169', 6, 9.1 );
INSERT INTO DIEMSO(ID, SINHVIEN, MONHOC, DIEM) VALUES (7, '1412169', 7, 9.2 );
INSERT INTO DIEMSO(ID, SINHVIEN, MONHOC, DIEM) VALUES (8, '1412169', 8, 9.3 );
INSERT INTO DIEMSO(ID, SINHVIEN, MONHOC, DIEM) VALUES (9, '1412169', 9, 9.4 );

INSERT INTO SINHVIEN(MSSV, TENSINHVIEN, FACEBOOKID) VALUES ('1412169', N'Hồ Thảo Hiền',null);

INSERT INTO CACHDANHGIA(ID, MONHOC, CACHDANHGIA) VALUES (1, 1, N'Cách đánh giá môn học 1');
INSERT INTO CACHDANHGIA(ID, MONHOC, CACHDANHGIA) VALUES (2, 2, N'Cách đánh giá môn học 2');
INSERT INTO CACHDANHGIA(ID, MONHOC, CACHDANHGIA) VALUES (3, 3, N'Cách đánh giá môn học 3');
INSERT INTO CACHDANHGIA(ID, MONHOC, CACHDANHGIA) VALUES (4, 4, N'Cách đánh giá môn học 4');
INSERT INTO CACHDANHGIA(ID, MONHOC, CACHDANHGIA) VALUES (5, 5, N'Cách đánh giá môn học 5');
INSERT INTO CACHDANHGIA(ID, MONHOC, CACHDANHGIA) VALUES (6, 6, N'Cách đánh giá môn học 6');
INSERT INTO CACHDANHGIA(ID, MONHOC, CACHDANHGIA) VALUES (7, 7, N'Cách đánh giá môn học 7');
INSERT INTO CACHDANHGIA(ID, MONHOC, CACHDANHGIA) VALUES (8, 8, N'Cách đánh giá môn học 8');
INSERT INTO CACHDANHGIA(ID, MONHOC, CACHDANHGIA) VALUES (9, 9, N'Cách đánh giá môn học 9');

INSERT INTO MONHOCTHAYTHE(MONHOC, MONHOCTHAYTHE) VALUES (1, 2);
INSERT INTO MONHOCTHAYTHE(MONHOC, MONHOCTHAYTHE) VALUES (1, 3);
INSERT INTO MONHOCTHAYTHE(MONHOC, MONHOCTHAYTHE) VALUES (1, 4);
INSERT INTO MONHOCTHAYTHE(MONHOC, MONHOCTHAYTHE) VALUES (2, 1);
INSERT INTO MONHOCTHAYTHE(MONHOC, MONHOCTHAYTHE) VALUES (3, 1);
INSERT INTO MONHOCTHAYTHE(MONHOC, MONHOCTHAYTHE) VALUES (4, 1);

INSERT INTO MONHOCTRUOC(MONHOC, MONHOCTRUOC) VALUES (1, 5);
INSERT INTO MONHOCTRUOC(MONHOC, MONHOCTRUOC) VALUES (1, 6);
INSERT INTO MONHOCTRUOC(MONHOC, MONHOCTRUOC) VALUES (1, 7);
INSERT INTO MONHOCTRUOC(MONHOC, MONHOCTRUOC) VALUES (2, 8);
INSERT INTO MONHOCTRUOC(MONHOC, MONHOCTRUOC) VALUES (2, 9);

INSERT INTO SINHVIEN_MONHOC(ID, SINHVIEN, MONHOC) VALUES (1, '1412169', 1);
INSERT INTO SINHVIEN_MONHOC(ID, SINHVIEN, MONHOC) VALUES (2, '1412169', 2);
INSERT INTO SINHVIEN_MONHOC(ID, SINHVIEN, MONHOC) VALUES (3, '1412169', 3);
INSERT INTO SINHVIEN_MONHOC(ID, SINHVIEN, MONHOC) VALUES (4, '1412169', 4);
INSERT INTO SINHVIEN_MONHOC(ID, SINHVIEN, MONHOC) VALUES (5, '1412169', 5);
INSERT INTO SINHVIEN_MONHOC(ID, SINHVIEN, MONHOC) VALUES (6, '1412169', 6);
INSERT INTO SINHVIEN_MONHOC(ID, SINHVIEN, MONHOC) VALUES (7, '1412169', 7);
INSERT INTO SINHVIEN_MONHOC(ID, SINHVIEN, MONHOC) VALUES (8, '1412169', 8);
INSERT INTO SINHVIEN_MONHOC(ID, SINHVIEN, MONHOC) VALUES (9, '1412169', 9);

INSERT INTO CHUYENNGANH_MONHOC (ID, CHUYENNGANH, MONHOC, HINHTHUCMONHOC) VALUES (1, 2, 1, 1);
INSERT INTO CHUYENNGANH_MONHOC (ID, CHUYENNGANH, MONHOC, HINHTHUCMONHOC) VALUES (2, 4, 5, 1);
INSERT INTO CHUYENNGANH_MONHOC (ID, CHUYENNGANH, MONHOC, HINHTHUCMONHOC) VALUES (3, 4, 8, 1);
INSERT INTO CHUYENNGANH_MONHOC (ID, CHUYENNGANH, MONHOC, HINHTHUCMONHOC) VALUES (4, 1, 1, 2);
INSERT INTO CHUYENNGANH_MONHOC (ID, CHUYENNGANH, MONHOC, HINHTHUCMONHOC) VALUES (5, 2, 1, 3);
INSERT INTO CHUYENNGANH_MONHOC (ID, CHUYENNGANH, MONHOC, HINHTHUCMONHOC) VALUES (6, 2, 1, 4);
INSERT INTO CHUYENNGANH_MONHOC (ID, CHUYENNGANH, MONHOC, HINHTHUCMONHOC) VALUES (7, 4, 1, 7);
INSERT INTO CHUYENNGANH_MONHOC (ID, CHUYENNGANH, MONHOC, HINHTHUCMONHOC) VALUES (8, 4, 1, 9);


-------------------------------------------------------------------------------------------------------------

select ndmh.noidungmonhoc from noidungmonhoc as ndmh,monhoc as mh where ndmh.monhoc = mh.id and mh.tenmonhoc like ten_mon_hoc
select tlmh.tentailieu,tlmh.tailieu from tailieumonhoc as tlmh, monhoc as mh where tlmh.monhoc = mh.id and mh.tenmonhoc like ten_mon_hoc
select mh.tenmonhoc from monhoc as mh where mh.id in (select distinct mht.monhoctruoc from monhoc as mh1, monhoctruoc as mht where mh1.id = mht.monhoc and mh1.tenmonhoc like ten_mon_hoc)
select mh.tenmonhoc from monhoc as mh where mh.id in (select distinct mht.monhocthaythe from monhoc as mh1, monhocthaythe as mht where mh1.id = mht.monhoc and mh1.tenmonhoc like ten_mon_hoc)
select mh.tenmonhoc from monhoc as mh, chuyennganh as cn, chuyennganh_monhoc as cnmh where cnmh.chuyennganh = cn.id and cnmh.monhoc = mh.id and cn.tenchuyennganh like ten_chuyen_nganh and cnmh.hinhthucmonhoc = hinh_thuc_mon_hoc
select * from nganh as n,chuyennganh as cn where n.id = cn.nganh and cn.tenchuyennganh like ten_chuyen_nganh
select mh.tenmonhoc, ndcbh.thoigian, ndcbh.noidungbuoihoc from noidungcacbuoihoc as ndcbh, monhoc as mh where ndcbh.monhoc = mh.id and ndcbh.thoigian > DATE(current_timestamp)
select mh.tenmonhoc, ds.diem from diemso as ds, monhoc as mh where ds.monhoc = mh.id and ds.sinhvien like ma_so_sinh_vien and mh.tenmonhoc like ten_mon_hoc
select cdg.cachdanhgia from cachdanhgia as cdg, monhoc as mh where cdg.monhoc = mh.id and mh.tenmonhoc like ten_mon_hoc
select bt.noidungbaitap from baitap as bt, monhoc as mh where bt.monhoc = mh.id and mh.tenmonhoc like ten_mon_hoc


INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(1, 1, 'thong_tin_khac', null);
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(2, 2, 'noi_dung_mon_hoc', 'select ndmh.noidungmonhoc from noidungmonhoc as ndmh,monhoc as mh where ndmh.monhoc = mh.id and mh.tenmonhoc like ten_mon_hoc');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(3, 2, 'tai_lieu_mon_hoc', 'select tlmh.tentailieu,tlmh.tailieu from tailieumonhoc as tlmh, monhoc as mh where tlmh.monhoc = mh.id and mh.tenmonhoc like ten_mon_hoc');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(4, 2, 'mon_hoc_truoc', 'select mh.tenmonhoc from monhoc as mh where mh.id in (select distinct mht.monhoctruoc from monhoc as mh1, monhoctruoc as mht where mh1.id = mht.monhoc and mh1.tenmonhoc like ten_mon_hoc)');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(5, 2, 'mon_hoc_thay_the', 'select mh.tenmonhoc from monhoc as mh where mh.id in (select distinct mht.monhocthaythe from monhoc as mh1, monhocthaythe as mht where mh1.id = mht.monhoc and mh1.tenmonhoc like ten_mon_hoc)');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(6, 2, 'so_luong_mon_hoc_chuyen_nganh', 'select mh.tenmonhoc from monhoc as mh, chuyennganh as cn, chuyennganh_monhoc as cnmh where cnmh.chuyennganh = cn.id and cnmh.monhoc = mh.id and cn.tenchuyennganh like ten_chuyen_nganh and cnmh.hinhthucmonhoc = hinh_thuc_mon_hoc');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(7, 2, 'so_luong_chuyen_nganh', 'select * from nganh as n,chuyennganh as cn where n.id = cn.nganh and cn.tenchuyennganh like ten_chuyen_nganh');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(8, 2, 'noi_dung_buoi_hoc_tiep_theo', 'select mh.tenmonhoc, ndcbh.thoigian, ndcbh.noidungbuoihoc from noidungcacbuoihoc as ndcbh, monhoc as mh where ndcbh.monhoc = mh.id and ndcbh.thoigian > DATE(current_timestamp)');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(9, 2, 'diem_so', 'select mh.tenmonhoc, ds.diem from diemso as ds, monhoc as mh where ds.monhoc = mh.id and ds.sinhvien like ma_so_sinh_vien and mh.tenmonhoc like ten_mon_hoc');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(10, 2, 'cach_danh_gia', 'select cdg.cachdanhgia from cachdanhgia as cdg, monhoc as mh where cdg.monhoc = mh.id and mh.tenmonhoc like ten_mon_hoc');
INSERT INTO INTENT_RESPONSE(ID, INTENTTYPE, INTENT, RESPONSE) VALUES(12, 2, 'so_luong_bai_tap', 'select bt.noidungbaitap from baitap as bt, monhoc as mh where bt.monhoc = mh.id and mh.tenmonhoc like ten_mon_hoc');
