var mysql = require('mysql');
var conn = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database: 'study_management',
    port: 3311,
    connectionLimit : 20
});
conn.connect(function(err) {
    if (err) throw err;
    else console.log ("CONNECTED !!!")
});

module.exports = conn;