const API_AI_TOKEN = 'AI TOKEN';
const apiAiClient = require('apiai')(API_AI_TOKEN);
const FACEBOOK_ACCESS_TOKEN = '<ACCESS TOKEN>';
const API_GOOGLE_SEARCH = 'GOOGLE API TOKEN'
const db = require('./database');

var google = require('google');
var logger = require('morgan');
var http = require('http');
var bodyParser = require('body-parser');
var express = require('express');
var router = express();

var app = express();
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
var server = http.createServer(app);
var request = require("request");

// Create request, if the request is created successfully, it will return 200 OK message. 
app.get('/', (req, res) => {
  res.send("Home page. Server running okay.");
});

// Create webhook (sample code)
app.get('/webhook', function(req, res) {
    const hubChallenge = req.query['hub.challenge'];
    const hubMode = req.query['hub.mode'];
    const verifyTokenMatches = (req.query['hub.verify_token'] === 'rukawa10051996');
    if (hubMode && verifyTokenMatches) {
        res.status(200).send(hubChallenge);
    } else {
        res.status(403).end();
    }
});

// Process messenger from user
app.post('/webhook', function(req, res) {
  var entries = req.body.entry;
  console.log (entries)
  for (var entry of entries) {
    var messaging = entry.messaging;
    for (var message of messaging) {
      var senderId = message.sender.id;
      if (message.message) {
        if (message.message.text) {
            // Send users' query to dialogflow
            const apiaiSession = apiAiClient.textRequest(message.message.text, {sessionId: 'rukawa10051996'});
            // Send request action to user
            sendTypingActionToUser(senderId)
            
            // Return message from dialogflow
            apiaiSession.on('response', (response) => {
                // GET result returned from Dialogflow
                messageABC =  defaultMessage = response.result.fulfillment.messages
                console.log (messageABC)
                console.log(response.result)
                console.log(response.result.fulfillment.messages)
                resolvedQuery = response.result.resolvedQuery;
                response.result.resolvedQuery;
                speech = response.result.fulfillment.speech;
                intentName = response.result.metadata.intentName
                param = response.result.parameters
                defaultMessage = ''
                facebookMessage = ''
                defaultMessage = response.result.fulfillment.messages[0].speech
                if (response.result.fulfillment.messages.length > 1){
                    facebookMessage = response.result.fulfillment.messages[1].speech
                }
                var paramName = Object.keys(param).map(function(value, index) {
                    value.index = index;
                    return value;
                });
                var paramValue = Object.keys(param).map(function (key) {
                    return param[key]; 
                })

                if (speech === ''){
                    //Send promp to get enough parameter
                    if (facebookMessage === ''){
                        sendTextMessageToUser(senderId,defaultMessage);
                        console.log ('defaultMessage: ', defaultMessage)
                    }
                    //Send response
                    else {
                        sendTextMessageToUser(senderId, facebookMessage);
                        console.log ('facebookMessage: ', facebookMessage)
                    }
                    responseToUser(senderId, resolvedQuery, speech, intentName, paramName, paramValue);
                }
                else {
                    sendTextMessageToUser(senderId, speech);
                    console.log ('speech: ', speech)
                }
            });

            apiaiSession.on('error', error => console.log(error));
            apiaiSession.end();
        }
      }
    }
  }

  res.status(200).send("OK");
});


// TRả về kết quả dạng text cho facebook. để làm phần này, m tham khảo thêm messenger API
function sendTextMessageToUser(senderId, text) {
    request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: { access_token: FACEBOOK_ACCESS_TOKEN },
        method: 'POST',
        json: {
            recipient: { id: senderId },
            message: { text },
        }
    });
}

// Return result under payload format
function sendPayloadToUser(senderId, attachment) {
    request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: { access_token: FACEBOOK_ACCESS_TOKEN },
        method: 'POST',
        json: {
            recipient: { id: senderId },
            message: { attachment },
        }
    });
}

// Simulate typing action
function sendTypingActionToUser (senderId){
    request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: { access_token: FACEBOOK_ACCESS_TOKEN },
        method: 'POST',
        json: {
            recipient: { id: senderId },
            sender_action: "typing_on"
        }
    });
}

// GET google lookup results
google.resultsPerPage = 1
function sendGoogleSearchResultToUser (senderId, userQuery){
     google( userQuery + 'hcmus ' , function (err, res){
        if (err) throw err
        console.log ('userQuery: ' , userQuery)
        console.log (res.links)
        if (res.links.length === 0){
            sendTextMessageToUser(senderId, 'Xin lỗi, tôi không tìm thấy câu trả lời cho câu hỏi này của bạn. Tôi rất lấy làm tiếc!');
        }
        else {
            if ((res.links[0].href !== null && res.links[0].href.includes('https://www.hcmus.edu.vn/'))){
                // Return facebook's payload under whitelist format
                 sendPayloadToUser (senderId,  {"type": "template",
                                    "payload": {
                                        "template_type": "generic",
                                        "elements": [{
                                            "title": res.links[0].title,
                                            "subtitle": res.links[0].description,
                                            "default_action": {
                                                "type": "web_url",
                                                "url": res.links[0].href,
                                                "messenger_extensions": true,
                                                "webview_height_ratio": "tall",
                                                "fallback_url": res.links[0].href
                                            }
                                        }]
                                    }
                })
            }
            else{
            // if the result is not in whitelist, it would be returned under plain text.
               sendTextMessageToUser(senderId, res.links[0].href);
            }
        }
       
    })
}

function confirmActionInfo (senderId){
    sendPayloadToUser (senderId,  {
                        "type":"template",
                        "payload":{
                            "template_type":"button",
                            "text":"MSSV: 1412169 \nĐăng kí môn học: ABC XYZ \nVui lòng xác nhận thông tin.",
                            "buttons":[
                            {
                                "type":"postback",
                                "title":"Xác nhận",
                                "payload": "Ok"
                            },
                             {
                                "type":"postback",
                                "title":"Thông tin sai",
                                "payload": "Fail"
                            }
                            ]
                        }
    })
    
}

// thay các param trong câu query thành các giá trị thực thế để thực hiện truy vấn 
function replaceParamValueInQuery (sqlString, paramName, paramValue){
    for (i = 0; i< paramName.length; i++){
        if (paramValue[i][0] === 'undefined' || paramValue[i][0] === ''){
            sqlString = sqlString.replace(paramName[i], null)
        }
        else {
            sqlString = sqlString.replace(paramName[i], "N'%" + paramValue[i][0] + "%'") 
        }
    }
    return sqlString
}


// xét từng loại intent, nếu loại 0-> ko trả về gì hết, loại 1-> trả về thẳng kq từ gg, loại 2-> trả về kq từ db, neus ko có kq trong db, thì tìm gg rồi trả về, loại 3-> thực hiện hành động giúp người dùng, gọi procedure
function responseToUser (senderId, resolvedQuery, speech, intentName, paramName, paramValue) {
        // setTimeout(sendTextMessageToUser, 50000, senderId, 'Có vẻ bạn không có câu hỏi cho mình. Mình kết thúc cuộc trò chuyện ở đây nhé. Khi nào có câu hỏi, bạn có thể quay lại hỏi mình bất cứ lúc nào. Chào bạn'); 
        var sql = "select intentType, response as res from intent_response where intent = ?";
        // console.log ('userQuery: ', userQuery)
        db.query(sql, [intentName], function(err, response) {
            if (err) throw err;
            Object.keys(response).map(function(key) {
                var row = response[key];
                console.log ('Đây là row response ', row.res)
                 //Sending no result
                if (row.intentType === 0){
                    sendTextMessageToUser(senderId, '');
                }
                //Sending result directly from internet
                else if (row.intentType === 1){ 
                    sendGoogleSearchResultToUser (senderId, resolvedQuery)
                    setTimeout(sendTextMessageToUser, 5000, senderId, 'Bạn có muốn hỏi câu hỏi nào khác không?'); 
                }
                //Sending result from Database
                else if (row.intentType === 2) {
                    db.query(replaceParamValueInQuery(row.res, paramName, paramValue), function(err, results) {
                        console.log ('Ket qua tra ve tu db', results)
                        if (err) throw err;
                        if (results.length === 0){
                                console.log ('trường hợp 2')
                                //No result available in database, sending result directly from internet
                                sendGoogleSearchResultToUser (senderId, resolvedQuery)
                                setTimeout(sendTextMessageToUser, 5000, senderId, 'Bạn có muốn hỏi câu hỏi nào khác không?'); 
                             
                        }
                        else {
                            Object.keys(results).forEach(function (key) {
                                row = results[key]
                                var temp = Object.keys(row).map(function (key) {
                                    return row[key]; 
                                })
                                dbResult = ""
                                temp.forEach(function(field){
                                    console.log ('field:' ,field)
                                    dbResult += field + '\n';
                                })
                                sendTextMessageToUser(senderId, dbResult);
                                setTimeout(sendTextMessageToUser, 5000, senderId, 'Bạn có muốn hỏi câu hỏi nào khác không?'); 
                            })
                        }
                    })
                }
                //Doing action
                else {
                    //Confirm information
                    confirmActionInfo(senderId)
                    sendTextMessageToUser(senderId, "Yêu cầu của bạn đã được thực hiện.");
                }
            });   
        })
}

app.set('port', process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8080);
app.set('ip', process.env.OPENSHIFT_NODEJS_IP || process.env.IP || "127.0.0.1");

server.listen(app.get('port'), app.get('ip'), function() {
  console.log("Chat bot server listening at %s:%d ", app.get('ip'), app.get('port'));
});
