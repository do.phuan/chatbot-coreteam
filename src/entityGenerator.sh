#!/bin/bash 

# Initialization
work_dir=`pwd`
mkdir -p ../output/agent/entities
path_to_training_src=""

if [ $# -eq 0 ]
then
    path_to_training_src="../input/mobile_entities.csv"
else 
    path_to_training_src="$1"
fi 

entityName=""

# Required Functions
function createJsonConfigFile4Entity {
    if [ $1 != "" ]
    then 
        entityName=`echo $1 | awk '{ split($0, ar_1, ","); 
                                    print ar_1[1]; }'`

        # TODO: Create config json file 
      echo "{
    \"automatedExpansion\": true,
    \"isEnum\": false,
    \"isOverridable\": true,
    \"name\": \"${entityName}\"
    }" > ../output/agent/entities/${entityName}.json
    
    else 
      echo "Failed to create config file"
    fi 
}

function createSynonyms {
    if [ $1 != "" ]
    then 
        # keyValue is the key using in the last line of json file
        # synonyms is the list of synonyms of keyValue
        line=$1
        # keyValue=`echo $line | awk '{
        #                         split($0, ar_1, ","); 
        #                         split(ar_1[2],ar_2,";"); 
        #                         print ar_2[1]; 
        #                         }'`

        # synonyms=`echo $line | awk '{
        #                                 ar_2[1]="\""ar_2[1];
        #                                 split(ar_2[1],ar_3,";")
        #                                 gsub(";","\", \"",ar_1[2]);
        #                                 gsub("\" ","\"",ar_1[2]);
        #                                 gsub(" && ","\",\"",ar_1[2]);
        #                                 print ar_1[2]"\""; }'`
        nbSplitTime=`echo $line | awk '{
                                            splitTime=split($0,ar_1,";")
                                            print splitTime }'`
        for i in 1..${nbSplitTime}
        do
            synonyms=`echo $line`
        done
        echo "[
        {
          \"synonyms\": [
              ${synonyms}
          ],
          \"value\": \"${keyValue}\"
        }
        ]" >> ../output/agent/entities/${entityName}_entries_en.json
        
        echo "Done" > ../output/entity_status_ok
    else 
        echo "Failed to create synonym list"
    fi  
}

function createPackageJson {
    echo "{
      \"version\": \"1.0.0\"
    }" > ../output/agent/package.json
}

# Main program
createPackageJson 
IFS=$'\n'
while read line 
do 
    createJsonConfigFile4Entity $line
    createSynonyms $line
done < ${path_to_training_src}