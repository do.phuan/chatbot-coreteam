#!/usr/bin/python
# -*- coding: utf-8 -*-
from sys import argv
script, intentFile= argv
import json
import shutil
import codecs
import os
import csv
import json
from pprint import pprint

def readFile(intentFile):
    intentList = []
    reader = codecs.open(intentFile, 'r', encoding='utf-8')
    for line in reader:
        intent = []
        for item in line.split(','):
            intent.append(item.replace('\r\n','').replace('\n',''))
        intentList.append(intent)
    reader.close()
    return intentList

def writeFile(intentList):
	with open('../input/sample_intent.json') as sampleIntentFile:
		sampleJson = json.load(sampleIntentFile)
	sampleIntentFile.close()

	path = "../output"
	os.chdir(path)

	path = "./agent/intents"
	if os.path.exists(path):
		shutil.rmtree(path)
	os.mkdir( path )
	os.chdir(path)

	for i in range (1, len(intentList)):
		jsonIntent = sampleJson
		jsonIntent["responses"][0]["parameters"]=[]
		jsonIntent["responses"][0]["messages"]=[]
		jsonIntent["name"] = intentList[i][1]
		parameterList = []
		unrequiredParameterList = []
		unrequiredParameterList = intentList[i][2].split(';')
		parameterList = intentList[i][3].split(';')
		promptsList = []
		promptsList = intentList[i][4].split(';')
		response = intentList[i][5]
		intentFileName = intentList[i][1] + ".json"
		intentFile = codecs.open(intentFileName, 'w', encoding='utf-8')

		if (response != '' and intentList[i][0] == '1'):
			defaultResponse = {"type": 0,
					          "lang": "en",
					          "speech": response.replace('"','')}
			facebookResponse = {"type": 0,
					          "platform": "facebook",
					          "lang": "en",
					          "speech": response.replace('"','')}
			jsonIntent["responses"][0]["messages"].append(defaultResponse)
			jsonIntent["responses"][0]["messages"].append(facebookResponse)

		for k in range(0,len(parameterList)):
			if (parameterList[k] != '' and intentList[i][3] != ''):
				prompt = [{"lang": "en",
			              "value": promptsList[k-1]}]
				param = {"required": True,
		            	"dataType": "@" + parameterList[k],
		                "name": parameterList[k],
		                "value": "$" + parameterList[k],
		                "isList": True,
		                "prompts": prompt}
				jsonIntent["responses"][0]["parameters"].append(param)

		for j in range(0,len(unrequiredParameterList)):
			if (unrequiredParameterList[j] != '' and intentList[i][2] != ''):
				param1 = {"dataType": "@" + unrequiredParameterList[j],
							"name": unrequiredParameterList[j],
							"value": "$" + unrequiredParameterList[j],
							"isList": True}  
				jsonIntent["responses"][0]["parameters"].append(param1)

		json.dump(jsonIntent, intentFile, ensure_ascii=False, sort_keys=True, indent=2, separators=(',', ': '))
		intentFile.close()
		
		


if __name__ == '__main__':
    intentList = readFile(intentFile)
    writeFile(intentList)
