#!/usr/bin/python
from sys import argv
script, entityFile = argv
import json
import codecs
import os
import shutil

def readFile(entityFile):
    optionList = []
    inputFile = codecs.open(entityFile, encoding='utf-8')
    for line in inputFile:
        option = []
        for item in line.split(","):
            option.append(item.replace('\r\n','').replace('\n',''))
        optionList.append(option)
    inputFile.close()
    for x in optionList:
        print (x)
    return optionList


def writeFile(optionList):
    path = "../output"
    if os.path.exists(path):
        shutil.rmtree(path)
    os.mkdir( path)
    os.chdir(path)

    path = "./agent"
    if os.path.exists(path):
        shutil.rmtree(path)
    os.mkdir( path )
    os.chdir(path)

    version={"version":"1.0.0"}
    packageJsonFile = codecs.open("package.json", 'w', encoding='utf-8')
    json.dump(version, packageJsonFile, ensure_ascii=False, sort_keys=True, indent=2, separators=(',', ': '))
    packageJsonFile.close()
        
    path = "./entities"
    if os.path.exists(path):
        shutil.rmtree(path)
    os.mkdir( path )
    os.chdir(path)
    
    entryList = []
    for item in optionList:
        print (item)
        value = {"name": item[0],"isOverridable": True,"isEnum": False,"automatedExpansion": False}
        valueFileName = item[0] + ".json"
        valueFile = codecs.open(valueFileName, 'w', encoding='utf-8')
        json.dump(value, valueFile, ensure_ascii=False, sort_keys=True, indent=2, separators=(',', ': '))
        valueFile.close()

        synonymsFileName = item[0] + "_entries_en.json"
        synonymsFile = codecs.open(synonymsFileName, 'w', encoding='utf-8')
        wordList = item[1].split(";")
        print(wordList)
        valueList=[]
        for word in wordList:
        	value = {"value": word,"synonyms": [word]}
        	valueList.append(value)
        json.dump(valueList, synonymsFile, ensure_ascii=False, sort_keys=True, indent=2, separators=(',', ': '))
        synonymsFile.close()

if __name__ == '__main__':
    optionList = readFile(entityFile)
    writeFile(optionList)


