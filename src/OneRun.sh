#!/bin/bash

opt=""
entityInputDir=""
intentInputDir=""
sentenceInputDir=""
line=""
name=""
function setEnv
{
    #rm -rf ../output/
    mkdir  -p ../output/agent/ ../output/sentence/
}

function generateEntities
{
    if [ ! -s ../entityGenerator.sh ]
    then 
        python ../src/gen_entity.py "${entityInputDir}"
        echo "Done" > ../output/entity_status_ok
    else 
        ../src/entityGenerator.sh "${entityInputDir}"
    fi
}

function generateIntents
{
    python ../src/gen_intent.py "${intentInputDir}"
    echo "Done" > ../output/intent_status_ok
}

function generateSentences
{
    python3 ../src/gen_sen.py "${sentenceInputDir}"
    echo "Done" > ../output/sentence_status_ok
}

function updateSynonyms
{
    res=`echo $line | awk '{
                        split($0,ar_1,",");
                        ar_1[3]="\""ar_1[3]"\""
                        gsub(";","\", \"",ar_1[3]);
                        print "{\n\t\"value\": \""ar_1[2]"\",\n\t\"synonyms\": [\n\t\t"ar_1[3]"\n\t]\n}";
                        }'`
    echo $res >> ../output/agent/$name.json
}
# Main program 
setEnv
while getopts "e:i:s:u:" opts; do 
    case ${opts} in 
        e)
            if [ "${OPTARG}" != "" ]
            then  
                entityInputDir="${OPTARG}"
                let "shift=shift+1"
                generateEntities 
            fi 
        ;;

        i)
            if [ "${OPTARG}" != "" ]
            then  
                intentInputDir="${OPTARG}"
                let "shift=shift+1"
                generateIntents 
            fi
        ;;

        s )
            if [ "${OPTARG}" != "" ]
            then  
                sentenceInputDir="${OPTARG}"
                let "shift=shift+1"
                generateSentences 
            fi
        ;;
        u)
            training_src=${OPTARG}
            echo $training_src
            name="phone_name_entries_en"
            echo "[" >> ../output/agent/$name.json
            while read l; do 
                line=$l
                echo "," >> ../output/agent/$name.json
                updateSynonyms
            done < ${OPTARG}
            echo "]" >> ../output/agent/$name.json
        ;;
    esac 
done 

# Create zip file of agent
zip -r ../output/agent_training.zip ../output/agent/