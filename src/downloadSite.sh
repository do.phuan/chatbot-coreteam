#!/bin/bash 

work_dir=`pwd`

rm -rf ../site 
mkdir -p ../site


. ${work_dir}/list_useragent.sh
u_a=0
maxLoops=5
loop=0
TAB="   "

#----------------------------------------------------------------------------------------
# @params: 
# link : Home page web address.
#----------------------------------------------------------------------------------------
function  downloadHomePage {
    
    link=$1

    while [ ${loop} -lt ${maxLoops} ]
    do 
        if [ ! -s ${work_dir}/home-page.html ]
        then 
            wget -c -nv --timeout=15 --tries=5 --waitretry=1 -U"${USERAGENT_ARR[$u_a]}" --random-wait --keep-session-cookies --save-cookies=${work_dir}/${d}/cookies.$$ ${link} -O ${work_dir}/home-page.html
        fi
        # Check if the file was downloaded successfully
        grep -i "<\/html>" ${work_dir}/home-page.html
        if [ $? -ne 0 ]
        then
            rm -f ${work_dir}/home-page.html
            let "loop=loop+1"
            echo "Re-download home page..."
        else
            loop=${maxLoops}
            echo "Download home page finished!"
        fi 
    done 
}

function extractLinksFromHomePage {
    awk -f ${work_dir}/linkExtractor.awk ${work_dir}/home-page.html > ../site/all_link.csv
}

function removeTempFiles {
    rm -rf ${work_dir}/home-page.html
}

downloadHomePage "https://www.hcmus.edu.vn/sinh-vien"
extractLinksFromHomePage
removeTempFiles 

# Return execution status.
if [ ! -s ../site/all_link.csv ]
then 
    echo "Failed to crawl data!" > ../output/status_crawl_failed
else
    echo "Download successfully!"  > ../output/status_crawl_ok
fi 