#!/usr/bin/python
# -*- coding: utf-8 -*-
from sys import argv
script, inputFile = argv
import spintax
import codecs
import os
import shutil
import csv

if __name__ == '__main__':
	regrexFile = codecs.open(inputFile, 'r', encoding='utf-8')
	path = "../output"
	os.chdir(path)
	path = "./sentence"
	if os.path.exists(path):
		shutil.rmtree(path)
	os.mkdir(path)
	os.chdir(path)
	sentenceFile = codecs.open('sentence.txt', 'w', encoding='utf-8')
	for line in regrexFile:
		lineTemp = line
		wordList = lineTemp.split('} {')
		count = 1;
		for item in wordList:
			count = count * (item.count('|')+1)
		if (count > 10):
			count = 10
		for i in range(0,3):
			sentenceFile.write(spintax.spin(line))
	regrexFile.close()
	sentenceFile.close()
    

