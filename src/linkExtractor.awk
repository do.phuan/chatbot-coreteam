BEGIN {
    i=1;
    c=0;
    title[i]="POST_TITLE"; i++;
    title[i]="LINK"; i++;
    max_i=i;
}

# Get link of Annoucements, Mid-term exam schedule, mid-term test results, re-judged test. 
/<span class="feed-link">/{
    c++
    getline
    split($0,ar_1,"\"")
    val["LINK",c]=trim(ar_1[2])

    getline
    gsub(/<[^>]+>/,"",$0)
    val["POST_TITLE",c]=trim($0)
}

# Get links of the other articles. (The one that has right direction arrow)
/<i class="fa fa-arrow-circle-right"><\/i>/{
    c++
    getline 
    split($0,ar_2,"\"")
    val["LINK",c]="https://www.hcmus.edu.vn"trim(ar_2[4])
    gsub(/<[^>]+>/,"",$0)
    val["POST_TITLE",c]=trim($0)
}

END {
    max_c=c 
    for(c=1;c<=max_c;c++) {
        if (clean(val["POST_TITLE",c]) != "" ) {
                for (i=1; i<max_i;i++) {
                    printf ("%s\t", clean(val[title[i],c]))
                }
                printf ("\n" )
        }
    }
}

function ltrim(s){
    gsub(/\s+$/,"",s);
    return s;
}

function rtrim(s){
    gsub(/^\s+/,"",s);
    return s;
}

function trim(s){
    return ltrim(rtrim(s));
}

function clean(txt) {
        sub("[\t ]+$", "", txt)
        sub("^[\t ]+", "", txt)
        sub("^M", "", txt)
        return txt
}